package agh.ics.oop;

public enum MapDirection {
    NORTH("Północ", "^", "up.png", new Vector2D(0, 1)),
    EAST("Wschód", ">", "right.png", new Vector2D(1, 0)),
    SOUTH("Południe", "v", "down.png", new Vector2D(0, -1)),
    WEST("Zachód", "&lt;", "left.png", new Vector2D(-1, 0));

    private final String full_name, shortcut, path;
    private final Vector2D unit_vector;
    private static MapDirection[] values = values();

    MapDirection(String full_name, String shortcut, String file_name, Vector2D unit_vector) {
        this.full_name = full_name;
        this.shortcut = shortcut;
        this.path = Utils.MAIN_PATH + file_name;
        this.unit_vector = unit_vector;
    }

    @Override
    public String toString() {
        return this.full_name;
    }

    public MapDirection next() {
        return values[(this.ordinal()+1)%values.length];
    }

    public MapDirection previous(){
        return values[(this.ordinal()+3)% values.length];
    }

    public Vector2D toUnitVector() {
        return this.unit_vector;
    }

    public String getShortcut() {
        return shortcut;
    }

    public String getPath() {
        return path;
    }
}
