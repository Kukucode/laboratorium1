package agh.ics.oop;

import agh.ics.oop.map.IEngine;
import agh.ics.oop.map.IMapUpdateObserver;
import agh.ics.oop.map.IWorldMap;
import agh.ics.oop.map.elements.SuperAnimal;
import agh.ics.oop.map.types.SimulationMap;

import java.util.ArrayList;

public class SuperSimulationEngine implements IEngine, Runnable {

    private final SimulationMap map;
    private ArrayList<IMapUpdateObserver> observers = new ArrayList<>();

    public SuperSimulationEngine(SimulationMap map) {
        this.map = map;
    }

    boolean isRunning = false;
    private SuperAnimal observedAnimal;

    @Override
    public void run() {
        isRunning = true;
        while(isRunning) {
            ArrayList<SuperAnimal> animalsToRemove = new ArrayList<>();
            for(SuperAnimal a : this.map.getAnimals()) {
                a.setEnergy(a.getEnergy()-1);
                if(a.getEnergy() <= 0) {
                    animalsToRemove.add(a);
                }else {
                    a.move();
                }
            }
            for(SuperAnimal a : animalsToRemove) {
                a.setDeathDate(map.getDay());
                this.map.addToTotalLivespan(a.getLifespan());
                map.removeAnimal(a);
            }
            this.map.addToTotalDeaths(animalsToRemove.size());

            this.map.getAnimals().forEach((a) -> a.eat());
            this.map.getAnimals().forEach((a) -> a.breed());
            this.map.getAnimals().forEach((a) -> a.setHasBreededThisDay(false));
            this.map.generateAnimalsInQueue();
            this.map.spawnFood(10);
            this.map.setNextDay();
            this.notifyObservers();
        }
    }

    public void registerObserver(IMapUpdateObserver observer) {
        this.observers.add(observer);
    }

    public void notifyObservers() {
        for(IMapUpdateObserver observer : observers) {
            observer.mapUpdated();
        }
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public SuperAnimal getObservedAnimal() {
        return observedAnimal;
    }

    public void setObservedAnimal(SuperAnimal observedAnimal) {
        this.observedAnimal = observedAnimal;
    }
}
