package agh.ics.oop;

public class Utils {

    public static String MAIN_PATH = "src/main/resources/";

    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static boolean getRandomBoolean(float probabilityOfTrue) {
        return Math.random() < probabilityOfTrue;
    }

}
