package agh.ics.oop;

import agh.ics.oop.gui.App;
import agh.ics.oop.gui.SuperApp;
import agh.ics.oop.map.types.GrassField;
import agh.ics.oop.map.IEngine;
import agh.ics.oop.map.IWorldMap;
import agh.ics.oop.map.types.RectangularMap;
import javafx.application.Application;

import javax.swing.*;
import java.awt.*;

public class World {

    public static void main(String[] args){

        System.out.println("Running simulation project...");

        Application.launch(SuperApp.class, args);

        System.out.println("Stop");
    }

    public static void run(MoveDirection[] steps) {
        for(MoveDirection a : steps) {
            System.out.print("Zwierzak idzie ");
            System.out.print(switch (a){
                case LEFT -> "w lewo";
                case RIGHT -> "w prawo";
                case FORWARD -> "do przodu";
                case BACKWARD -> "do tylu";
            });
            System.out.println();
        }
    }

    public static MoveDirection[] convertToEnum(String[] args) {
        int n = args.length;

        MoveDirection[] d = new MoveDirection[n];
        for (int i = 0; i < n; i++) {
            d[i] = switch (args[i]) {
                case "f" -> MoveDirection.FORWARD;
                case "b" -> MoveDirection.BACKWARD;
                case "r" -> MoveDirection.RIGHT;
                case "l" -> MoveDirection.LEFT;
                default -> throw new IllegalStateException("Unexpected value: " + args[i]);
            };
        }
        return d;
    }

    public void lab123(String[] args) {
        //        System.out.println("Start");
//
//        MoveDirection[] pass = convertToEnum(args);
//        run(pass);
//
//        System.out.println("Lab 2");
//
//        Vector2D position1 = new Vector2D(1,2);
//        System.out.println(position1);
//        Vector2D position2 = new Vector2D(-2,1);
//        System.out.println(position2);
//        System.out.println(position1.add(position2));
//
//        MapDirection lewo = MapDirection.WEST;
//        System.out.println(lewo.next().previous());
//
//        System.out.println("--------------");
//
//        System.out.println("Lab 3");
//        Animal a1 = new Animal(MapDirection.NORTH, new Vector2D(2, 2));
//
//        MoveDirection[] mds = OptionsParser.parse(args);
////        a1.move(mds);
//
//        System.out.println(a1);
    }

    public void lab4(String[] args) {
        MoveDirection[] directions = new OptionsParser().parse(args);

        IWorldMap map = new RectangularMap(10, 5);
        Vector2D[] positions = { new Vector2D(2,2), new Vector2D(3,4) };
        IEngine engine = new SimulationEngine(directions, map, positions);

        JFrame frame = new JFrame("WorldSimulation");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel();
        frame.setSize(500, 500);
        label.setText("Loading");

        JButton jButton = new JButton("Run");
        jButton.setBounds(50,100,60,30);
        jButton.addActionListener(e -> {
            engine.run();
            label.setText("<html>" + map.toString().replace("\n", "<br>"));
            System.out.println(map);

        });

        frame.add(jButton);
        frame.add(label);
        frame.setVisible(true);
    }

    public static void lab5(String[] args) {
        MoveDirection[] directions = new OptionsParser().parse(args);

        IWorldMap map = new GrassField(10);
        JFrame frame = new JFrame("GrassField");
        Vector2D[] positions = { new Vector2D(2,2), new Vector2D(3,4)};

        SimulationEngine engine = new SimulationEngine(directions, map, positions);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel();
        frame.setSize(500, 500);

        System.out.println(map);
        label.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 18));
        label.setText("<html>" + map.toString().replace("\n", "<br>"));

        JButton jButton = new JButton("Run");
        jButton.setBounds(50,100,60,30);
        jButton.addActionListener(e -> {
            engine.run();
            label.setText("<html>" + map.toString().replace("\n", "<br>"));
            System.out.println(map);

        });

        frame.add(jButton);
        frame.add(label);
        frame.setVisible(true);
    }
}
