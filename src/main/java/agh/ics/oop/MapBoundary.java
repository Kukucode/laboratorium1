package agh.ics.oop;

import agh.ics.oop.map.IPositionChangeObserver;

import java.util.SortedSet;
import java.util.TreeSet;

public class MapBoundary implements IPositionChangeObserver {

    final SortedSet<Vector2D> xAxis = new TreeSet<>((o1, o2) -> {
        if (o1.x < o2.x) {
            return -1;
        }else if(o1.x > o2.x){
            return 1;
        }
        return 0;
    });

    final SortedSet<Vector2D> yAxis = new TreeSet<>((o1, o2) -> {
        if (o1.y < o2.y) {
            return -1;
        }else if(o1.y > o2.y){
            return 1;
        }
        return 0;
    });

    public MapBoundary() {

    }

    public void addElement(Vector2D newPosition) {
        xAxis.add(newPosition);
        yAxis.add(newPosition);
    }

    public Vector2D getMinimum() {
        return new Vector2D(xAxis.first().x, yAxis.first().y);
    }

    public Vector2D getMaximum() {
        return new Vector2D(xAxis.last().x, yAxis.last().y);
    }

    @Override
    public void positionChanged(Vector2D oldPosition, Vector2D newPosition) {
        xAxis.remove(oldPosition);
        yAxis.remove(oldPosition);
        xAxis.add(newPosition);
        yAxis.add(newPosition);
    }

}
