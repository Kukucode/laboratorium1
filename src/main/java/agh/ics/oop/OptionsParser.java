package agh.ics.oop;

public class OptionsParser {

    public static MoveDirection[] parse(String[] args) {
        MoveDirection[] tempRes = new MoveDirection[args.length];

        int currentIndex = 0;

        for(String arg : args) {
            MoveDirection md = MoveDirection.parse(arg);
            if(md != null) {
                tempRes[currentIndex] = md;
                currentIndex++;
            }
        }

        MoveDirection[] res = new MoveDirection[currentIndex];
        for(int i = 0; i < currentIndex; i++) {
            res[i] = tempRes[i];
        }

        return res;
    }

}
