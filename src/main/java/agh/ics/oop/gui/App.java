package agh.ics.oop.gui;

import agh.ics.oop.*;
import agh.ics.oop.map.*;
import agh.ics.oop.map.elements.IMapElement;
import agh.ics.oop.map.types.GrassField;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.FileNotFoundException;

public class App extends Application implements IMapUpdateObserver{

    IWorldMap map;
    GridPane gp = new GridPane();
    int moveDelay = 1000;
    SimulationEngine engine;

    @Override
    public void start(Stage primaryStage) throws Exception {

        HBox hBox = new HBox();
        VBox leftBox = new VBox();
        leftBox.getChildren().add(new Label("Simulation Engine"));
        leftBox.setMinWidth(100);

        Button startButton = new Button("Start");

        TextField textField = new TextField();
        leftBox.getChildren().add(startButton);
        leftBox.getChildren().add(textField);

        startButton.setOnAction(event -> {
            engine.setMoveDirections(OptionsParser.parse(textField.getText().split(" ")));

            Thread engineThread = new Thread(engine);
            engineThread.start();
        });

        hBox.getChildren().add(leftBox);
        hBox.getChildren().add(gp);

        Scene scene = new Scene(hBox, 800, 500);
        primaryStage.setScene(scene);
        primaryStage.show();
        Platform.runLater(() -> {
            try {
                generateMap();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    public void init() throws Exception {
        super.init();

        this.map = new GrassField(20);
        Vector2D[] positions = { new Vector2D(2,2), new Vector2D(3,4)};

        MoveDirection[] directions = new OptionsParser().parse(getParameters().getRaw().toArray(String[]::new));
        engine = new SimulationEngine(directions, map, positions);
        engine.registerObserver(this);
    }

    public void generateMap() throws FileNotFoundException {
        GrassField gf = (GrassField) map;
        gp.getChildren().clear();

        Vector2D min = gf.getMapBoundary().getMinimum();
        Vector2D max = gf.getMapBoundary().getMaximum();

        for (int x = 0; x < max.x-min.x + 2; x++) {
            for (int y = 0; y < max.y-min.y + 2; y++) {
                Label label1 = new Label("");
                Vector2D current = new Vector2D(x+min.x-1, max.y-y+1);
                if (x == 0 && y != 0) {
                    label1 = new Label(" " + current.y + " ");
                }
                if (y == 0 && x != 0) {
                    label1 = new Label(" " + current.x + " ");
                }
                if (map.isOccupied(current)) {
                    VBox vb = new GuiElementBox(null, (IMapElement) map.objectAt(current), null).getvBox();
                    gp.add(vb, x, y, 1, 1);
                }else {
                    gp.add(label1, x, y, 1, 1);
                }
                GridPane.setHalignment(label1, HPos.CENTER);
            }
        }
        gp.add(new Label("y/x"), 0, 0, 1, 1);
        gp.getColumnConstraints().add(new ColumnConstraints(30));
        gp.getRowConstraints().add(new RowConstraints(30));
        gp.setPrefSize(20, 20);
        gp.setGridLinesVisible(true);
    }

    @Override
    public void mapUpdated() {
        Platform.runLater(() -> {
            try {
                generateMap();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        try {
            Thread.sleep(this.moveDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
