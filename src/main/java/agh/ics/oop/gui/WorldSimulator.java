package agh.ics.oop.gui;

import agh.ics.oop.SuperSimulationEngine;
import agh.ics.oop.Vector2D;
import agh.ics.oop.map.IMapUpdateObserver;
import agh.ics.oop.map.elements.Grass;
import agh.ics.oop.map.elements.IMapElement;
import agh.ics.oop.map.elements.SuperAnimal;
import agh.ics.oop.map.types.SimulationMap;
import javafx.application.Platform;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.Optional;

public class WorldSimulator implements IMapUpdateObserver {

    private final SimulationMap simulationMap;
    private final SuperSimulationEngine simulationEngine;
    private final VBox container;
    private final GridPane mapGui;
    private boolean isRunning = false;
    private Thread currentThread = null;

    private XYChart.Series dataSeries1, dataSeries2;

    private Text currentAnimals, currentGrass, averageLifespan;
    private VBox animalDetails;
    private final int delayInMillis;

    public WorldSimulator(SimulationMap simulationMap, SuperSimulationEngine simulationEngine, int delayInMillis) {
        this.simulationMap = simulationMap;
        this.simulationEngine = simulationEngine;
        this.delayInMillis = delayInMillis;

        this.simulationEngine.registerObserver(this);

        container = new VBox();

        VBox leftPanel = new VBox();
        leftPanel.getChildren().add(new Text("Solid walls: " + this.simulationMap.isSolidWalls()));

        CheckBox checkBox = new CheckBox("Is magic map?");
        leftPanel.getChildren().add(checkBox);

        Button runPauseButton = new Button("Start/stop");
        runPauseButton.setOnAction((v) -> {
            isRunning = !isRunning;
            runPauseButton.setText(isRunning ? "Stop" : "Start");
            if(isRunning) {
                currentThread = new Thread(this.simulationEngine);
                currentThread.start();
            }else{
                this.simulationEngine.setRunning(false);
                currentThread.interrupt();
            }
        });
        leftPanel.getChildren().add(runPauseButton);

        Button addAnimalButton = new Button("Add animal to map");
        addAnimalButton.setOnAction((v) -> {
            this.simulationMap.spawnAnimal(10);
            mapUpdated();
        });
        leftPanel.getChildren().add(addAnimalButton);

        currentAnimals = new Text("Animals: -1");
        currentGrass = new Text("Grass: -1");
        averageLifespan = new Text();

        leftPanel.getChildren().add(currentAnimals);
        leftPanel.getChildren().add(currentGrass);
        leftPanel.getChildren().add(averageLifespan);

        leftPanel.getChildren().add(prepareChart());
        updateAnimalDetailedBox(null);
        leftPanel.getChildren().add(this.animalDetails);

        mapGui = new GridPane();
        mapGui.setVgap(1);
        mapGui.setHgap(1);
        mapGui.setPrefSize(15, 15);
        generateMapGui();
        refreshMapGui();

        container.getChildren().add(leftPanel);
        container.getChildren().add(mapGui);

    }

    public VBox getContainer() {
        return container;
    }

    private void generateMapGui() {
        ColumnConstraints columnConstraints = new ColumnConstraints(15);
        for(int i = 0; i < simulationMap.getWidth(); i++) {
            mapGui.getColumnConstraints().add(columnConstraints);
        }
        RowConstraints rowConstraints = new RowConstraints(15);
        for(int i = 0; i < simulationMap.getHeight(); i++) {
            mapGui.getRowConstraints().add(rowConstraints);
        }
    }

    private void refreshMapGui() {
        mapGui.getChildren().clear();
        for(int x = 0; x < simulationMap.getWidth(); x++) {
            for (int y = 0; y < simulationMap.getHeight(); y++) {
                Vector2D pos = new Vector2D(x, y);
                if(simulationMap.getMapElementsAt(pos).size() > 0) {
                    Optional<IMapElement> mapElement = simulationMap.getMapElementsAt(pos).stream().max(Comparator.comparing(IMapElement::getViewPriority));
                    try {
                        if(mapElement.isPresent()) {
                            VBox vb = new GuiElementBox(this, mapElement.get(), this.simulationEngine).getvBox();
                            mapGui.add(vb, x, y);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }else {
                    if (simulationMap.isGrassOnPosition(pos)) {
                        VBox vb = null;
                        try {
                            vb = new GuiElementBox(this, new Grass(pos), this.simulationEngine).getvBox();
                            mapGui.add(vb, x, y);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }else {
                        ImageView imageView = new ImageView(simulationMap.isJungle(pos) ? GuiUtils.getNotGrassJungle() : GuiUtils.getNotGrass());
                        imageView.setFitHeight(20);
                        imageView.setFitWidth(20);
                        imageView.setOpacity(simulationMap.isJungle(pos) ? 0.5f : 0.2f);
                        mapGui.add(imageView, x, y);
                    }
                }
            }
        }
    }

    private ScatterChart prepareChart() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Day");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Amount");

        ScatterChart scatterChart = new ScatterChart(xAxis, yAxis);

        scatterChart.setPrefSize(500, 300);

        dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Animals");

        dataSeries2 = new XYChart.Series();
        dataSeries2.setName("Grass");

        scatterChart.getData().add(dataSeries1);
        scatterChart.getData().add(dataSeries2);
        return scatterChart;
    }

    @Override
    public void mapUpdated() {
        Platform.runLater(() -> {
            refreshMapGui();
            int animals = this.simulationMap.getAnimals().size();
            int grass = this.simulationMap.getGrassAmount();
            dataSeries1.getData().add(new XYChart.Data(this.simulationMap.getDay(), animals));
            dataSeries2.getData().add(new XYChart.Data(this.simulationMap.getDay(), grass));

            this.currentAnimals.setText("Animals: " + animals);
            this.currentGrass.setText("Grass: " + grass);
            this.averageLifespan.setText("Avg lifespan: " + simulationMap.getAverageLifespan());

            this.updateAnimalDetailedBox(this.simulationEngine.getObservedAnimal());
        });
        try {
            Thread.sleep(delayInMillis);
        } catch (InterruptedException e) {
            System.out.println("Symulacja przerwana");
        }
    }

    public void updateAnimalDetailedBox(SuperAnimal superAnimal) {
        if (animalDetails == null) {
            this.animalDetails = new VBox();
        }
        if (superAnimal == null) {
            return;
        }
        this.animalDetails.getChildren().clear();

        Label animalLabel = new Label("Observing #" + superAnimal.getId() + " ("+superAnimal.getPosition()+")");
        this.animalDetails.getChildren().add(animalLabel);

        Label energyLabel = new Label("Energy " + superAnimal.getEnergy());
        this.animalDetails.getChildren().add(energyLabel);

        Label bornDateLabel = new Label("Born date " + superAnimal.getBornDate() + " death: " + superAnimal.getDeathDate());
        this.animalDetails.getChildren().add(bornDateLabel);

        Label childrenLabel = new Label("Total children: " + superAnimal.getChildrenAmount() + ", descendants (from now): " + superAnimal.getMap().getObservedAnimalDescendantAmount());
        this.animalDetails.getChildren().add(childrenLabel);

        Label genotype = new Label("Genotype: " + superAnimal.getGenesList());
        this.animalDetails.getChildren().add(genotype);

    }

}
