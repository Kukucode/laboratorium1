package agh.ics.oop.gui;

import agh.ics.oop.SuperSimulationEngine;
import agh.ics.oop.map.elements.Grass;
import agh.ics.oop.map.elements.IMapElement;
import agh.ics.oop.map.elements.SuperAnimal;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.FileNotFoundException;

public class GuiElementBox {

    private final VBox vBox;

    public GuiElementBox(WorldSimulator worldSimulator, IMapElement element, SuperSimulationEngine simulationEngine) throws FileNotFoundException {
        StackPane pane = new StackPane();
        Image image = element.getImage();
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(10);
        imageView.setFitWidth(10);
        imageView.setOpacity(0.2);

        if(element instanceof Grass) {
            imageView.setOpacity(1);
        }

        pane.getChildren().add(imageView);
        pane.getChildren().add(getLabel(element));

        vBox = new VBox();
        vBox.getChildren().add(pane);
        vBox.setAlignment(Pos.CENTER);

        if (element instanceof SuperAnimal) {
            SuperAnimal superAnimal = (SuperAnimal) element;
            vBox.setOnMouseClicked((event) -> {
                simulationEngine.setObservedAnimal(superAnimal);
                System.out.println("Wybrano do obserwowania" + superAnimal.getId() + " --- " +
                        superAnimal.getMap().getMapElementsAt(superAnimal.getPosition()).size());
                worldSimulator.updateAnimalDetailedBox(superAnimal);
                superAnimal.getMap().setObservedAnimalDescendantAmount(0);
                superAnimal.getMap().getAnimals().forEach((s) -> s.setObserved(false));
                superAnimal.setObserved(true);
            });
        }
    }

    private Text getLabel(IMapElement element) {
        String label = " ";
        if(element instanceof SuperAnimal){
            SuperAnimal superAnimal = (SuperAnimal) element;
            label = Math.min(superAnimal.getEnergy(), 99) + "";
        }
        Text text = new Text(label);
        Font f = new Font("Arial", 8);
        text.setFill(Color.GREEN);
        text.setFont(f);
        return text;
    }

    public VBox getvBox() {
        return vBox;
    }
}
