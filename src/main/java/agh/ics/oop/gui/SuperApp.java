package agh.ics.oop.gui;

import agh.ics.oop.*;
import agh.ics.oop.map.types.SimulationMap;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;


/**
 * Do dodania
 * - jedzenie dostaja te co maja najwiecej energii na danym polu,
 * - spawnienie jedzenia ZAWSZE,
 * - początkowo na jednym polu zawsze jedno zwierze,
 * - dominujacy genotyp
 *
 *
 *
 */

public class SuperApp extends Application {

    @Override
    public void start(Stage primaryStage) {

        VBox vBox = new VBox();
        Label l = new Label("SimulationEngine v0.1");
        l.setFont(new Font("Arial", 24));
        primaryStage.setTitle("Simulation engine");

        Label widthLabel = new Label("Width:");
        TextField mapWidth = new TextField("30");

        Label heightLabel = new Label("Height:");
        TextField mapHeight = new TextField("15");

        Label startEnergyLabel = new Label("Energy:");
        TextField startEnergy = new TextField("30");

        Label startAmountOfFoodLabel = new Label("Start grass amount:");
        TextField startAmountOfFood = new TextField("30");

        Label startAnimalsAmountLabel = new Label("Start animals amount:");
        TextField startAnimalsAmount = new TextField("20");

        Label dayDurationLabel = new Label("Day duration (in millis):");
        TextField dayDuration = new TextField("100");


        Label jungleLabel = new Label("Ile procent dzungli?");
        Slider jungleSlider = new Slider(0, 100, 10);
        jungleSlider.setMajorTickUnit(10);
        jungleSlider.setMinorTickCount(5);
        jungleSlider.setShowTickLabels(true);
        jungleSlider.setShowTickMarks(true);
        jungleSlider.setSnapToTicks(true);


        Button startSimulation = new Button("Run simulation");

        startSimulation.setOnAction((value) -> {
            System.out.println("Running our simulation...");

            int width = Integer.valueOf(mapWidth.getText());
            int height = Integer.valueOf(mapHeight.getText());
            double jungleRatio = jungleSlider.getValue();
            int startFood = Integer.valueOf(startAmountOfFood.getText());
            int startAnimals = Integer.valueOf(startAnimalsAmount.getText());
            int startEnergyValue = Integer.valueOf(startEnergy.getText());
            int dayDurationValue = Integer.valueOf(dayDuration.getText());
            HBox maps = new HBox();

            WorldSimulator top = this.getWorldSimulator(width, height, jungleRatio, startFood, startAnimals, startEnergyValue, dayDurationValue, false);
            WorldSimulator down = this.getWorldSimulator(width, height, jungleRatio, startFood, startAnimals, startEnergyValue, dayDurationValue, true);

            maps.getChildren().addAll(top.getContainer(), down.getContainer());

            Scene s2 = new Scene(maps, 700, 500);
            primaryStage.setScene(s2);

        });

        startSimulation.fire();

        vBox.getChildren().add(l);
        vBox.getChildren().add(widthLabel);
        vBox.getChildren().add(mapWidth);

        vBox.getChildren().add(heightLabel);
        vBox.getChildren().add(mapHeight);

        vBox.getChildren().add(startEnergyLabel);
        vBox.getChildren().add(startEnergy);

        vBox.getChildren().add(startAmountOfFoodLabel);
        vBox.getChildren().add(startAmountOfFood);

        vBox.getChildren().add(startAnimalsAmountLabel);
        vBox.getChildren().add(startAnimalsAmount );

        vBox.getChildren().add(dayDurationLabel);
        vBox.getChildren().add(dayDuration);

        vBox.getChildren().add(jungleLabel);
        vBox.getChildren().add(jungleSlider);

        vBox.getChildren().add(startSimulation);


        Scene scene = new Scene(vBox, 800, 500);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private WorldSimulator getWorldSimulator(int mapWidth, int mapHeight, double jungleRatio, int startAmountOfFood, int startAnimalsAmount, int startEnergy, int dayDuration, boolean solidWalls) {
        SimulationMap simulationMap = new SimulationMap(mapWidth, mapHeight, (float) jungleRatio/100f, solidWalls);
        SuperSimulationEngine simulationEngine = new SuperSimulationEngine(simulationMap);
        simulationMap.spawnFood(startAmountOfFood);
        simulationMap.spawnManyAnimals(startAnimalsAmount, startEnergy);
        WorldSimulator appSimulation = new WorldSimulator(simulationMap, simulationEngine, dayDuration);
        return appSimulation;
    }
}
