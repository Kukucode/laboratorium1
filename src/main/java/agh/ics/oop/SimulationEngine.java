package agh.ics.oop;

import agh.ics.oop.map.*;
import agh.ics.oop.map.elements.Animal;
import agh.ics.oop.map.elements.Grass;
import agh.ics.oop.map.types.GrassField;

import java.util.ArrayList;

public class SimulationEngine implements IEngine, Runnable {

    private MoveDirection[] moveDirections;
    private final IWorldMap map;
    private int lastIndex = 0;
    private ArrayList<IMapUpdateObserver> observers = new ArrayList<>();

    public SimulationEngine(IWorldMap map, Vector2D[] animalsPositions) {
        this(null, map, animalsPositions);
    }

    public SimulationEngine(MoveDirection[] moveDirections, IWorldMap map, Vector2D[] animalsPositions) {
        this.moveDirections = moveDirections;
        this.map = map;

        for(Vector2D v : animalsPositions) {
            Animal a = new Animal(map, MapDirection.NORTH, v);
            map.place(a);
        }
    }

    @Override
    public void run() {
        while (lastIndex < this.moveDirections.length) {
            for(Animal a : map.getAnimals()) {
                MoveDirection md = null;
                if (lastIndex < this.moveDirections.length) {
                    md = this.moveDirections[lastIndex];
                    lastIndex += 1;
                }else {
                    md = MoveDirection.values()[Utils.getRandomNumber(0, MoveDirection.values().length-1)];
                }
                Vector2D new_pos = a.move(md, true);
                if (new_pos != null && map.isOccupied(new_pos)) {
                    if(map.objectAt(new_pos) instanceof Grass) {
                        map.removeObjectAt(new_pos);
                        if(map instanceof GrassField) {
                            GrassField gf = (GrassField) map;
                            gf.generateNewGrass();
                        }
                    }
                }
                a.move(md);
            }
            notifyObservers();
        }
    }

    public void setMoveDirections(MoveDirection[] moveDirections) {
        this.lastIndex = 0;
        this.moveDirections = moveDirections;
    }

    public void registerObserver(IMapUpdateObserver observer) {
        this.observers.add(observer);
    }

    public void notifyObservers() {
        for(IMapUpdateObserver observer : observers) {
            observer.mapUpdated();
        }
    }
}
