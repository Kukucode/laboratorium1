package agh.ics.oop;

import java.util.Arrays;

public enum MoveDirection {
    FORWARD(new String[]{"f", "forward"}),
    BACKWARD(new String[]{"b", "backward"}),
    RIGHT(new String[]{"r", "right"}),
    LEFT(new String[]{"l", "left"});

    private final String[] stringShortcuts;

    MoveDirection(String[] a) {
        this.stringShortcuts = a;
    }

    public static MoveDirection parse(String s) {
        for(MoveDirection md : MoveDirection.values()) {
            for(String toParse : md.stringShortcuts) {
                if (toParse.equals(s)) {
                    return md;
                }
            }
        }
        throw new IllegalArgumentException(s + " is not legal move specification");
    }

    @Override
    public String toString() {
        return "MoveDirection{" +
                this.name() +
                '}';
    }
}
