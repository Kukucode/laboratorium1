package agh.ics.oop.map;

import agh.ics.oop.map.elements.Animal;
import agh.ics.oop.map.elements.Grass;
import agh.ics.oop.Vector2D;
import agh.ics.oop.map.elements.IMapElement;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

abstract public class AbstractWorldMap implements IPositionChangeObserver{

    private HashMap<Vector2D, IMapElement> mapElements = new LinkedHashMap<>();

    public boolean isOccupied(Vector2D position) {
        return objectAt(position) != null;
    }

    public IMapElement objectAt(Vector2D position) {
        return mapElements.get(position);
    }

    public boolean place(IMapElement element) {
        if (this.isOccupied(element.getPosition()) && !(element instanceof Grass)) {
            throw new IllegalArgumentException("Position " + element.getPosition() + " is already occupied!");
        }
        this.mapElements.put(element.getPosition(), element);
        if (element instanceof Animal) {
            Animal a = (Animal) element;
            a.addObserver(this);
        }
        return true;
    }

    public void removeObjectAt(Vector2D position) {
        if(isOccupied(position)){
            mapElements.remove(position);
        }
    }

    public List<Animal> getAnimals() {
        return this.getMapElements().values().stream().filter(iMapElement -> iMapElement instanceof Animal).
                map(iMapElement -> (Animal) iMapElement).collect(Collectors.toList());
    }

    public HashMap<Vector2D, IMapElement> getMapElements() {
        return mapElements;
    }

    public void positionChanged(Vector2D oldPosition, Vector2D newPosition) {
        this.mapElements.put(newPosition, this.getMapElements().get(oldPosition));
        this.mapElements.remove(oldPosition);
    }
}
