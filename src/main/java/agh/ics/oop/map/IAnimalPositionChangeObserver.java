package agh.ics.oop.map;

import agh.ics.oop.Vector2D;
import agh.ics.oop.map.elements.IMapElement;
import agh.ics.oop.map.elements.SuperAnimal;

public interface IAnimalPositionChangeObserver {

    void positionChanged(IMapElement mapElement, Vector2D oldPosition);

}
