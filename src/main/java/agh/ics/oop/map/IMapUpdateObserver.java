package agh.ics.oop.map;

import agh.ics.oop.Vector2D;

public interface IMapUpdateObserver {

    void mapUpdated();

}
