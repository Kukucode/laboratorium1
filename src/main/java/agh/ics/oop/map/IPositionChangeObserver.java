package agh.ics.oop.map;

import agh.ics.oop.Vector2D;

public interface IPositionChangeObserver {

    /**
     * Metoda wywolywana jest w momencie w którym zmieniana jest pozycja Animal.
     * Informowane o tym sa klasy typu IWorldMap
     *
     * @param oldPosition Poprzednia pozycja obiektu na mapie
     * @param newPosition Nowa pozycja obiektu na mapie
     */
    void positionChanged(Vector2D oldPosition, Vector2D newPosition);

}
