package agh.ics.oop.map.types;

import agh.ics.oop.Vector2D;
import agh.ics.oop.map.AbstractWorldMap;
import agh.ics.oop.map.IWorldMap;
import agh.ics.oop.map.MapVisualizer;

public class RectangularMap extends AbstractWorldMap implements IWorldMap {

    private final int width, height;

    public RectangularMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public boolean canMoveTo(Vector2D position) {
        if (!(position.x <= width && position.x >= 0 && position.y <= height && position.y >= 0)) {
            return false;
        }
        if (isOccupied(position)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        MapVisualizer mapVisualizer = new MapVisualizer(this);
        return mapVisualizer.draw(new Vector2D(0, 0), new Vector2D(width, height));
    }

    @Override
    public void removeObjectAt(Vector2D position) {
        return;
    }
}
