package agh.ics.oop.map.types;

import agh.ics.oop.*;
import agh.ics.oop.map.AbstractWorldMap;
import agh.ics.oop.map.IWorldMap;
import agh.ics.oop.map.MapVisualizer;
import agh.ics.oop.map.elements.Animal;
import agh.ics.oop.map.elements.Grass;
import agh.ics.oop.map.elements.IMapElement;

public class GrassField extends AbstractWorldMap implements IWorldMap {

    private final int maxCoordinates;
    private final MapBoundary mapBoundary = new MapBoundary();

    public GrassField(int grassAmount) {
        maxCoordinates = (int) Math.sqrt(grassAmount*10);
        for (int i = 0; i < grassAmount; i++) {
            generateNewGrass(0, maxCoordinates);
        }
    }

    @Override
    public boolean place(IMapElement element) {
        if(element instanceof Animal) {
            this.mapBoundary.addElement(element.getPosition());
            ((Animal) element).addObserver(mapBoundary);
        }
        return super.place(element);
    }

    private void generateNewGrass(int minCoordinates, int maxCoordinates) {
        while(true) {
            int x = Utils.getRandomNumber(minCoordinates, maxCoordinates);
            int y = Utils.getRandomNumber(minCoordinates, maxCoordinates);
            Vector2D newPos = new Vector2D(x, y);
            if (!isOccupied(newPos)) {
                this.place(new Grass(newPos));
                this.mapBoundary.addElement(newPos);
                return;
            }
        }
    }

    public void generateNewGrass() {
        generateNewGrass(0, maxCoordinates);
    }

    @Override
    public boolean canMoveTo(Vector2D position) {
        if (objectAt(position) instanceof Animal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        int offset = 1;
        MapVisualizer mapVisualizer = new MapVisualizer(this);
        return mapVisualizer.draw(mapBoundary.getMinimum(), mapBoundary.getMaximum());
    }

    public MapBoundary getMapBoundary() {
        return mapBoundary;
    }
}
