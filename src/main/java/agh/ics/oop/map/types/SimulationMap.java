package agh.ics.oop.map.types;

import agh.ics.oop.Utils;
import agh.ics.oop.Vector2D;
import agh.ics.oop.map.IAnimalPositionChangeObserver;
import agh.ics.oop.map.elements.Grass;
import agh.ics.oop.map.elements.IMapElement;
import agh.ics.oop.map.elements.SuperAnimal;
import agh.ics.oop.map.elements.SuperAnimalRotation;

import java.util.*;

public class SimulationMap implements IAnimalPositionChangeObserver {

    private int width, height;
    float jungleRatio;

    Vector2D jungleBottomLeft, jungleTopRight;

    private final HashMap<Vector2D, HashSet<IMapElement>> elementsMap = new HashMap<>();
    private final boolean grassMap[][];
    private ArrayList<SuperAnimal> animals = new ArrayList<>();
    private ArrayList<SuperAnimal> historicalAnimals = new ArrayList<>();
    private ArrayList<SuperAnimal> animalsToGenerate = new ArrayList<>();
    private final boolean solidWalls;

    private int nextAnimalID = 0;
    private int day = 0;

    private int totalDeaths = 0, totalLivespan = 0;

    private int observedAnimalDescendantAmount = 0;

    public SimulationMap(int width, int height, float jungleRatio, boolean solidWalls) {
        this.width = width;
        this.height = height;
        this.jungleRatio = jungleRatio;
        this.solidWalls = solidWalls;
        this.grassMap = new boolean[width][height];
        this.jungleBottomLeft = new Vector2D((int) ((1-jungleRatio)*width/2), (int) ((1-jungleRatio)*height/2));
        this.jungleTopRight = new Vector2D((int) ((1+jungleRatio)*width/2), (int) ((1+jungleRatio)*height/2));
    }

    public SimulationMap(int width, int height, float jungleRatio) {
        this(width, height, jungleRatio, false);
    }

    public void spawnManyAnimals(int amount, int energy) {
        for(int i = 0; i < amount; i++) {
            spawnAnimal(energy);
        }
    }

    public void spawnAnimal(int energy){
        Vector2D pos = new Vector2D(Utils.getRandomNumber(0, width), Utils.getRandomNumber(0, height));
        SuperAnimalRotation rotation = SuperAnimalRotation.getRandom();
        SuperAnimal sa = new SuperAnimal(this, pos, energy, rotation);
        place(sa);
        animals.add(sa);
        historicalAnimals.add(sa);
        sa.registerObserver(this);
    }

    public void spawnAnimal(SuperAnimal animal){
        place(animal);
        animals.add(animal);
        historicalAnimals.add(animal);
        animal.registerObserver(this);
    }

    public void spawnFood(int amount) {
        for (int i = 0; i < amount/2; i++) {
            spawnJungleFood();
            spawnNonJungleFood();
        }
    }

    public void spawnJungleFood() {
        int x = Utils.getRandomNumber(jungleBottomLeft.x, jungleTopRight.x+1);
        int y = Utils.getRandomNumber(jungleBottomLeft.y, jungleTopRight.y+1);

        Vector2D pos = new Vector2D(x, y);

        if (getMapElementsAt(pos).size() == 0) {
            setGrassOnPosition(pos, true);
        }
    }

    public void spawnNonJungleFood() {
        int x = Utils.getRandomNumber(0, width);
        int y = Utils.getRandomNumber(0, height);

        Vector2D pos = new Vector2D(x, y);

        if (!isJungle(pos)) {
            if (getMapElementsAt(pos).size() == 0) {
                setGrassOnPosition(pos, true);
            }
        }
    }

    public boolean canMoveTo(Vector2D position) {
        if(position.x >= width || position.x < 0 || position.y < 0 || position.y >= height) {
            return false;
        }
        return true;
    }

    public void place(IMapElement mapElement) {
        if(mapElement instanceof Grass) {
            setGrassOnPosition(mapElement.getPosition(), true);
            return;
        }
        if (!elementsMap.containsKey(mapElement.getPosition())) {
            elementsMap.put(mapElement.getPosition(), new HashSet());
        }
        elementsMap.get(mapElement.getPosition()).add(mapElement);
    }

    public boolean isJungle(Vector2D position) {
        return position.follows(jungleBottomLeft) && position.precedes(jungleTopRight);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public HashSet<IMapElement> getMapElementsAt(Vector2D pos) {
        if (this.elementsMap.containsKey(pos)) {
            return this.elementsMap.get(pos);
        }
        return new HashSet<>();
    }

    public HashSet<SuperAnimal> getAnimalsAt(Vector2D pos) {
        HashSet<SuperAnimal> sa = new HashSet<>();
        for(IMapElement mapElement : getMapElementsAt(pos)) {
            if(mapElement instanceof SuperAnimal){
                sa.add((SuperAnimal) mapElement);
            }
        }
        return sa;
    }

    public ArrayList<SuperAnimal> getAnimals() {
        return animals;
    }

    @Override
    public void positionChanged(IMapElement mapElement, Vector2D oldPosition) {
        boolean removed = this.getMapElementsAt(oldPosition).remove(mapElement);
        place(mapElement);
    }

    public boolean isSolidWalls() {
        return solidWalls;
    }

    public void removeAnimal(SuperAnimal superAnimal) {
        this.animals.remove(superAnimal);
        this.getMapElementsAt(superAnimal.getPosition()).remove(superAnimal);
    }

    public boolean isGrassOnPosition(Vector2D pos) {
        return grassMap[pos.x][pos.y];
    }

    public void setGrassOnPosition(Vector2D pos, boolean value){
        grassMap[pos.x][pos.y] = value;
    }

    public int getGrassAmount() {
        int counter = 0;
        for(int i = 0; i < width; i++){
            for (int j = 0; j < height; j++) {
                if(grassMap[i][j]) counter++;
            }
        }
        return counter;
    }

    /**
     * Dodawanie do kolejki zwierzat nowo urodzonych. Po zakonczeniu dnia sa generowane a kolejka czyszczona
     * @param animal zwierze do dodania
     */
    public void addAnimalToGenerate(SuperAnimal animal) {
        this.animalsToGenerate.add(animal);
    }

    public void generateAnimalsInQueue() {
        for (SuperAnimal superAnimal : animalsToGenerate) {
            this.spawnAnimal(superAnimal);
        }
        this.animalsToGenerate.clear();
    }

    public int getNextAnimalID() {
        this.nextAnimalID += 1;
        return this.nextAnimalID;
    }

    public void setNextDay() {
        this.day++;
    }

    public int getDay() {
        return day;
    }

    public int getAverageLifespan() {
        return totalLivespan / totalDeaths;
    }

    public void addToTotalLivespan(int amount) {
        this.totalLivespan += amount;
    }

    public void addToTotalDeaths(int amount){
        this.totalDeaths += amount;
    }

    public int getObservedAnimalDescendantAmount() {
        return observedAnimalDescendantAmount;
    }

    public void increaseObservedAnimalDescendantAmount(int amount) {
        this.observedAnimalDescendantAmount += amount;
    }

    public void setObservedAnimalDescendantAmount(int observedAnimalDescendantAmount) {
        this.observedAnimalDescendantAmount = observedAnimalDescendantAmount;
    }
}
