package agh.ics.oop.map.elements;

import agh.ics.oop.Utils;
import agh.ics.oop.Vector2D;
import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Objects;

public class Grass implements IMapElement {

    private Vector2D position;
    private static Image image;
//    private final int id;

    public Grass(Vector2D position) {
        this.position = position;
    }

    public Vector2D getPosition() {
        return position;
    }

    @Override
    public boolean isAt(Vector2D position) {
        return position.equals(this.position);
    }

    @Override
    public String toString() {
        return "*";
    }

    @Override
    public String getImagePath() {
        return Utils.MAIN_PATH + "grass2.png";
    }

    @Override
    public Image getImage() {
        if (image == null) {
            try {
                image = new Image(new FileInputStream(this.getImagePath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return image;
    }

    @Override
    public int getViewPriority() {
        return -1;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Grass) {
            return 0;
        }
        return -1;
    }

}
