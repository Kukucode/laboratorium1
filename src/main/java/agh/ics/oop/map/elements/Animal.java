package agh.ics.oop.map.elements;

import agh.ics.oop.MapDirection;
import agh.ics.oop.MoveDirection;
import agh.ics.oop.Vector2D;
import agh.ics.oop.map.IPositionChangeObserver;
import agh.ics.oop.map.IWorldMap;
import javafx.scene.image.Image;

import java.util.ArrayList;

public class Animal implements IMapElement {

    private MapDirection orientation;
    private Vector2D position;
    private IWorldMap map;
    private ArrayList<IPositionChangeObserver> observers = new ArrayList<>();

    @Override
    public String toString() {
        return orientation.getShortcut();
    }

    public Animal(IWorldMap map, MapDirection orientation, Vector2D position) {
        this.map = map;
        this.orientation = orientation;
        this.position = position;
    }

    public Animal(MapDirection orientation, Vector2D position) {
        this(null, orientation, position);
    }

    public Animal(IWorldMap map) {
        this(map, null, null);
    }

    public Animal(IWorldMap map, Vector2D initialPosition) {
        this(map, null, initialPosition);
    }

    public void move(MoveDirection[] directions){
        for(MoveDirection md : directions) {
            this.move(md);
        }
    }

    public Vector2D move(MoveDirection direction) {
        return move(direction, false);
    }

    public Vector2D move(MoveDirection direction, boolean onlyGetPosition) {
        Vector2D newPosition = null;
        switch (direction){
            case RIGHT -> {
                if (!onlyGetPosition) { this.orientation = this.orientation.next();} ;
            }
            case LEFT -> {
                if (!onlyGetPosition) { this.orientation = this.orientation.previous();} ;
            }
            case FORWARD -> newPosition = this.position.add(this.orientation.toUnitVector());
            case BACKWARD -> newPosition = this.position.add(this.orientation.toUnitVector().opposite());
            default -> newPosition = new Vector2D(0, 0);
        }
        if(!onlyGetPosition && newPosition != null && map != null && map.canMoveTo(newPosition)) {
            this.positionChanged(this.position, newPosition);
            this.position = newPosition;
        }
        return newPosition;
    }

    public boolean isAt(Vector2D position) {
        return position.equals(this.position);
    }

    public MapDirection getOrientation() {
        return orientation;
    }

    public Vector2D getPosition() {
        return position;
    }

    public void addObserver(IPositionChangeObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(IPositionChangeObserver observer) {
        this.observers.remove(observer);
    }

    void positionChanged(Vector2D oldPos, Vector2D newPos) {
        for(IPositionChangeObserver observer : observers) {
            observer.positionChanged(oldPos, newPos);
        }
    }

    public String getImagePath() {
        return this.orientation.getPath();
    }

    @Override
    public Image getImage() {
        return null;
    }

    @Override
    public int getViewPriority() {
        return 0;
    }

    @Override
    public int compareTo(Object o) {
        return -1;
    }
}
