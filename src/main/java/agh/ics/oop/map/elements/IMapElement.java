package agh.ics.oop.map.elements;

import agh.ics.oop.Vector2D;
import javafx.scene.image.Image;

public interface IMapElement extends Comparable{

    Vector2D getPosition();
    boolean isAt(Vector2D position);
    String getImagePath();
    Image getImage();
    int getViewPriority();

}
