package agh.ics.oop.map.elements;

import agh.ics.oop.Utils;
import agh.ics.oop.Vector2D;
import agh.ics.oop.map.IAnimalPositionChangeObserver;
import agh.ics.oop.map.types.SimulationMap;
import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class SuperAnimal implements IMapElement {

    private final SimulationMap map;
    private Vector2D position;
    private int energy;
    private SuperAnimalRotation rotation;
    private final SuperAnimalRotation[] genes = new SuperAnimalRotation[32];
    private final ArrayList<IAnimalPositionChangeObserver> observers = new ArrayList<>();
    private boolean hasBreededThisDay = false, isObserved = false;
    private Image image;

    private final int id, bornDate;
    private int deathDate, childrenAmount = 0;

    public SuperAnimal(SimulationMap map, Vector2D position, int energy, SuperAnimalRotation rotation) {
        this.map = map;
        this.position = position;
        this.energy = energy;
        this.rotation = rotation;
        for(int i = 0; i < genes.length; i++) {
            genes[i] = SuperAnimalRotation.getRandom();
        }
        this.id = map.getNextAnimalID();
        this.bornDate = map.getDay();
        Arrays.sort(this.genes);
    }

    public SuperAnimal(SuperAnimal parent1, SuperAnimal parent2) {
        this.map = parent1.getMap();
        this.position = parent1.position;
        this.rotation = SuperAnimalRotation.getRandom();
        int e1 = parent1.getEnergy();
        int e2 = parent2.getEnergy();

        this.energy = (e1 + e2)/4;
        parent1.setEnergy((int) ((float) e1*(0.75f)));
        parent2.setEnergy((int) ((float) e2*(0.75f)));

        parent1.increaseChildrenAmount();
        parent2.increaseChildrenAmount();

        this.setObserved(parent1.isObserved() || parent2.isObserved());
        if(this.isObserved()) {
            map.increaseObservedAnimalDescendantAmount(1);
        }

        this.id = map.getNextAnimalID();
        this.bornDate = map.getDay();

        boolean p1left = Utils.getRandomBoolean(0.5f);

        int cut = (e1) * 32 / (e1+e2);

        if (p1left) {
            cut = 32 - cut;
        }

        for(int i = 0; i < genes.length; i++) {
            if (p1left ^ i < cut) {
                this.genes[i] = parent1.getGenes()[i];
            }else {
                this.genes[i] = parent2.getGenes()[i];
            }
        }

        hasBreededThisDay = true;
        parent1.hasBreededThisDay = true;
        parent2.hasBreededThisDay = true;

        Arrays.sort(this.genes);

    }

    public Vector2D getPosition() {
        return this.position;
    }

    public boolean isAt(Vector2D position) {
        return position.equals(this.position);
    }

    public String getImagePath() {
        return Utils.MAIN_PATH + "grass.png";
    }

    @Override
    public Image getImage() {
        if (this.image == null) {
            try {
                image = new Image(new FileInputStream(this.getImagePath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return this.image;
    }

    @Override
    public int getViewPriority() {
        return this.getEnergy();
    }

    public int compareTo(Object o) {
        if (o instanceof SuperAnimal){
            SuperAnimal sa = (SuperAnimal) o;
            if(sa.getEnergy() == this.getEnergy()) {
                return sa.getId() - this.getId();
            }
            return this.energy - sa.energy;
        }
        return 1;
    }

    public void move() {
        int moveNumber = Utils.getRandomNumber(0, 32);
        SuperAnimalRotation nextMove = genes[moveNumber];
        Vector2D newPosition = null;
        if(!nextMove.isMovingRotation()) {
            this.rotation.rotate(nextMove);
            return;
        }

        if (nextMove == SuperAnimalRotation.FRONT) {
            newPosition = this.position.add(rotation.getMovement());
        }else if (nextMove == SuperAnimalRotation.BACK) {
            newPosition = this.position.add(rotation.getMovement().opposite());
        }

        Vector2D oldPosition = this.position;
        if(map.isSolidWalls() && !map.canMoveTo(newPosition)) {
            System.out.println("Cannot move to " + newPosition);
            return;
        }
        System.out.println("solid: " + map.isSolidWalls() + " move to: " + map.canMoveTo(newPosition) + " " + newPosition);

        newPosition = new Vector2D(Math.floorMod(newPosition.x, map.getWidth()),
                Math.floorMod(newPosition.y, map.getHeight()));

        this.position = newPosition;
        System.out.println("Moving " + this.getId() + " from " + oldPosition + " to " + newPosition + " with energy: " + energy);
        notifyObservers(oldPosition);
    }

    public void eat() {
        if(!map.isGrassOnPosition(this.position)) return;
        HashSet<SuperAnimal> elementsAtPos = this.map.getAnimalsAt(this.position);
        Optional<SuperAnimal> mostEnergyAnimal = elementsAtPos.stream().max(Comparator.comparing(SuperAnimal::getEnergy));
        if(mostEnergyAnimal.isPresent()) {
            mostEnergyAnimal.get().addEnergy(20);
        }
        map.setGrassOnPosition(this.position, false);
    }

    public void breed() {
        HashSet<SuperAnimal> elementsAtPos = this.map.getAnimalsAt(this.position);
        if(elementsAtPos.size() < 2) return;
        List<SuperAnimal> sa = new ArrayList<>(elementsAtPos);
        Collections.sort(sa);
        SuperAnimal parent1 = sa.get(elementsAtPos.size()-1);
        SuperAnimal parent2 = sa.get(elementsAtPos.size()-2);

        if(parent1.hasBreededThisDay || parent2.hasBreededThisDay) return;
        if(parent1.getEnergy() < 5 || parent2.getEnergy() < 5) return;

        SuperAnimal child = new SuperAnimal(parent1, parent2);
        this.map.addAnimalToGenerate(child);

   }

    @Override
    public String toString() {
        return ""+this.energy;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public SuperAnimalRotation[] getGenes() {
        return genes;
    }

    public List getGenesList() {
        return Arrays.stream(this.getGenes()).toList();
    }

    public void registerObserver(IAnimalPositionChangeObserver observer) {
        this.observers.add(observer);
    }

    public void notifyObservers(Vector2D oldPosition) {
        for(IAnimalPositionChangeObserver observer : this.observers) {
            observer.positionChanged(this, oldPosition);
        }
    }

    public SimulationMap getMap() {
        return map;
    }

    public void addEnergy(int amount) {
        this.setEnergy(this.getEnergy()+amount);
    }

    public void setHasBreededThisDay(boolean hasBreededThisDay) {
        this.hasBreededThisDay = hasBreededThisDay;
    }

    public int getId() {
        return id;
    }

    public int getBornDate() {
        return bornDate;
    }

    public void setDeathDate(int deathDate) {
        this.deathDate = deathDate;
    }

    public int getDeathDate() {
        return deathDate;
    }

    public int getLifespan() {
        return Math.max(deathDate, map.getDay()) - bornDate;
    }

    public void increaseChildrenAmount() {
        this.childrenAmount++;
    }

    public int getChildrenAmount() {
        return childrenAmount;
    }

    public void setObserved(boolean observed) {
        isObserved = observed;
    }

    public boolean isObserved() {
        return isObserved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuperAnimal that = (SuperAnimal) o;
        return energy == that.energy && id == that.id;
    }



}
