package agh.ics.oop.map.elements;

import agh.ics.oop.MapDirection;
import agh.ics.oop.Utils;
import agh.ics.oop.Vector2D;

public enum SuperAnimalRotation {

    FRONT(new Vector2D(0, 1)),
    RIGHT_45(new Vector2D(1, 1)),
    RIGHT_90(new Vector2D(1, 0)),
    RIGHT_135(new Vector2D(1, -1)),
    BACK(new Vector2D(0, -1)),
    LEFT_135(new Vector2D(-1, -1)),
    LEFT_90(new Vector2D(-1, 0)),
    LEFT_45(new Vector2D(-1, 1));

    private final Vector2D movement;

    SuperAnimalRotation(Vector2D movement) {
        this.movement = movement;
    }

    private static SuperAnimalRotation[] values = values();

    public SuperAnimalRotation rotate(SuperAnimalRotation superAnimalRotation) {
        return values[(this.ordinal()+superAnimalRotation.ordinal())%values.length];
    }

    public static SuperAnimalRotation getRandom() {
        return values[Utils.getRandomNumber(0, values.length)];
    }

    public Vector2D getMovement() {
        return movement;
    }

    public boolean isMovingRotation(){
        return this == SuperAnimalRotation.FRONT || this == SuperAnimalRotation.BACK;
    }

    @Override
    public String toString() {
        return String.valueOf(this.ordinal());
    }
}
