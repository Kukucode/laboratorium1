package agh.ics.oop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Vector2DTest {

    @Test
    public void vectorTest() {

        Vector2D v1 = new Vector2D(1, 2);
        Vector2D v2 = new Vector2D(-3, -4);
        Vector2D v3 = new Vector2D(-5, 5);
        Vector2D v4 = new Vector2D(3, -3);

        assertEquals(v1, new Vector2D(1, 2));
        assertEquals("(-3,-4)", v2.toString());

        assertTrue(v2.precedes(v1));
        assertTrue(v1.follows(v2));

        assertEquals(new Vector2D(3, 5), v3.upperRight(v4));
        assertEquals(new Vector2D(-5, -3), v3.lowerRight(v4));

        assertEquals(new Vector2D(-2, -2), v1.add(v2));
        assertEquals(new Vector2D(4, 6), v1.substract(v2));
        assertEquals(new Vector2D(5, -5), v3.opposite());
    }

}
