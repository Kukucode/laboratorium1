package agh.ics.oop.project;

import agh.ics.oop.Vector2D;
import agh.ics.oop.map.elements.SuperAnimal;
import agh.ics.oop.map.elements.SuperAnimalRotation;
import agh.ics.oop.map.types.SimulationMap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SuperAnimalTest {

    @Test
    public void testNewChildBorn() {
        SimulationMap map = new SimulationMap(10, 10, 0.1f);
        SuperAnimal parent1 = new SuperAnimal(map, new Vector2D(5, 5), 20, SuperAnimalRotation.getRandom());
        SuperAnimal parent2 = new SuperAnimal(map, new Vector2D(5, 5), 20, SuperAnimalRotation.getRandom());

        SuperAnimal child = new SuperAnimal(parent1, parent2);
        assertEquals(15, parent1.getEnergy());
        assertEquals(15, parent2.getEnergy());
        assertEquals(10, child.getEnergy());

        assertEquals(parent1.getPosition(), child.getPosition());
        assertEquals(parent2.getPosition(), child.getPosition());

    }
}
