package agh.ics.oop.project;

import agh.ics.oop.MapDirection;
import agh.ics.oop.map.elements.SuperAnimalRotation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SuperAnimalRotationTest {

    @Test
    public void testRotationRight45() {
        assertEquals(SuperAnimalRotation.RIGHT_45, SuperAnimalRotation.FRONT.rotate(SuperAnimalRotation.RIGHT_45));
    }

    @Test
    public void testRotationLeft45() {
        assertEquals(SuperAnimalRotation.LEFT_45, SuperAnimalRotation.FRONT.rotate(SuperAnimalRotation.LEFT_45));
    }

    @Test
    public void testRotation90() {
        assertEquals(SuperAnimalRotation.BACK, SuperAnimalRotation.RIGHT_90.rotate(SuperAnimalRotation.RIGHT_90));
        assertEquals(SuperAnimalRotation.RIGHT_45, SuperAnimalRotation.LEFT_45.rotate(SuperAnimalRotation.RIGHT_90));
        assertEquals(SuperAnimalRotation.LEFT_45, SuperAnimalRotation.RIGHT_45.rotate(SuperAnimalRotation.LEFT_90));
    }

    @Test
    public void testRotation135() {
        assertEquals(SuperAnimalRotation.RIGHT_45, SuperAnimalRotation.LEFT_90.rotate(SuperAnimalRotation.RIGHT_135));
        assertEquals(SuperAnimalRotation.FRONT, SuperAnimalRotation.LEFT_135.rotate(SuperAnimalRotation.RIGHT_135));
    }

}
