package agh.ics.oop.project;

import agh.ics.oop.Utils;
import agh.ics.oop.Vector2D;
import agh.ics.oop.map.elements.Grass;
import agh.ics.oop.map.elements.SuperAnimalRotation;
import agh.ics.oop.map.types.SimulationMap;
import org.junit.jupiter.api.Test;

import java.util.SimpleTimeZone;

import static org.junit.jupiter.api.Assertions.*;

public class SimulationMapTest {

    @Test
    public void jungleLocationTest() {
        SimulationMap simulationMap = new SimulationMap(100, 30, 0.1f);
        assertTrue(simulationMap.isJungle(new Vector2D(50, 15)));
        assertTrue(simulationMap.isJungle(new Vector2D(45, 13)));
        assertTrue(simulationMap.isJungle(new Vector2D(55, 16)));
    }

    @Test
    public void placeGrassTest() {
        SimulationMap simulationMap = new SimulationMap(100, 30, 0.1f);
        Vector2D pos = new Vector2D(2, 2);
        Grass grass = new Grass(pos);
        simulationMap.place(grass);
        assertEquals(1, simulationMap.getGrassAmount());
        assertTrue(simulationMap.isGrassOnPosition(pos));
    }

    @Test
    public void placeRandomlyGrass() {
        SimulationMap simulationMap = new SimulationMap(100, 30, 0.1f);
        simulationMap.spawnFood(10);
        assertEquals(10, simulationMap.getGrassAmount());
    }

    @Test
    public void solidMapTest() {
        SimulationMap simulationMap = new SimulationMap(10, 10, 0.1f, true);

        // Pozycje poza mapa
        assertFalse(simulationMap.canMoveTo(new Vector2D(10, 10)));
        assertFalse(simulationMap.canMoveTo(new Vector2D(-1, 0)));
        assertFalse(simulationMap.canMoveTo(new Vector2D(10, 0)));

        // Pozycje poprawne
        assertTrue(simulationMap.canMoveTo(new Vector2D(5, 5)));
        assertTrue(simulationMap.canMoveTo(new Vector2D(0, 0)));
        assertTrue(simulationMap.canMoveTo(new Vector2D(9, 9)));
        assertTrue(simulationMap.canMoveTo(new Vector2D(0, 9)));
        assertTrue(simulationMap.canMoveTo(new Vector2D(9, 0)));
    }

}
