package agh.ics.oop;

import agh.ics.oop.map.elements.Animal;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnimalMoveTest {

    public void simpleTest() {
        Animal a1 = new Animal(MapDirection.NORTH, new Vector2D(2, 2));
        a1.move(OptionsParser.parse(new String[]{"r", "f", "f", "f"}));

        assertEquals(a1.getPosition(), new Vector2D(4, 2));
        assertEquals(a1.getOrientation(), MapDirection.EAST);
    }

    public void leftTest() {
        Animal a1 = new Animal(MapDirection.NORTH, new Vector2D(2, 2));
        a1.move(OptionsParser.parse(new String[]{"l", "f", "k"}));

        assertEquals(a1.getPosition(), new Vector2D(1, 2));
        assertEquals(a1.getOrientation(), MapDirection.WEST);
    }

    public void backwardTest() {
        Animal a1 = new Animal(MapDirection.NORTH, new Vector2D(2, 2));
        a1.move(OptionsParser.parse(new String[]{"b"}));

        assertEquals(a1.getPosition(), new Vector2D(2, 1));
        assertEquals(a1.getOrientation(), MapDirection.NORTH);
    }

    public void mapSizeTest() {
        Animal a1 = new Animal(MapDirection.NORTH, new Vector2D(2, 2));
        a1.move(OptionsParser.parse(new String[]{"f", "f", "f", "f"}));

        assertEquals(a1.getPosition(), new Vector2D(2, 4));
        assertEquals(a1.getOrientation(), MapDirection.NORTH);
    }

    public void leftBottomCornerTest() {
        Animal a1 = new Animal(MapDirection.NORTH, new Vector2D(2, 2));
        a1.move(OptionsParser.parse(new String[]{"l", "f", "f", "f", "l", "f", "f", "f", "f"}));

        assertEquals(a1.getPosition(), new Vector2D(0, 0));
        assertEquals(a1.getOrientation(), MapDirection.SOUTH);
    }

    public void righTopCornerTest() {
        Animal a1 = new Animal(MapDirection.NORTH, new Vector2D(2, 2));
        a1.move(OptionsParser.parse(new String[]{"f", "f", "f", "f", "r", "f", "f", "f", "f"}));

        assertEquals(a1.getPosition(), new Vector2D(4, 4));
        assertEquals(a1.getOrientation(), MapDirection.EAST);
    }

}
