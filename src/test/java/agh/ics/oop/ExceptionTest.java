package agh.ics.oop;

import agh.ics.oop.map.types.GrassField;
import agh.ics.oop.map.IWorldMap;
import agh.ics.oop.map.elements.Animal;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExceptionTest {

    @Test
    public void placeAlreadyOccupied() {
        Exception exception = assertThrows(Exception.class, () -> {
            IWorldMap map = new GrassField(10);
            Vector2D[] positions = { new Vector2D(2,2), new Vector2D(3,4) , new Vector2D(2, 2)};
            for(Vector2D pos : positions) {
                map.place(new Animal(map, pos));
            }
        });

        String expectedMessage = "occupied";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void wrongInputData() {
        Exception exception = assertThrows(Exception.class, () -> {
            IWorldMap map = new GrassField(10);
            Animal a1 = new Animal(map, new Vector2D(5, 5));
            a1.move(OptionsParser.parse(new String[]{"f", "f", "f", "o", "r", "f", "f", "f", "f"}));
        });

        String expectedMessage = "not legal";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}
