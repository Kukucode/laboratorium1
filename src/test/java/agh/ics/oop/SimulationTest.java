package agh.ics.oop;

import agh.ics.oop.map.IEngine;
import agh.ics.oop.map.IWorldMap;
import agh.ics.oop.map.types.RectangularMap;
import agh.ics.oop.map.elements.Animal;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SimulationTest {

    @Test
    public void basicTest() {
        MoveDirection[] directions = new OptionsParser().parse("f b r l f f r r f f f f f f f f".split(" "));
        IWorldMap map = new RectangularMap(10, 5);
        Vector2D[] positions = { new Vector2D(2,2), new Vector2D(3,4) };
        IEngine engine = new SimulationEngine(directions, map, positions);
        for (int i = 0; i < directions.length; i++) {
            System.out.println(map);
            engine.run();
        }
        RectangularMap rectangularMap = (RectangularMap) map;
        for(Animal a : rectangularMap.getAnimals()) {
            System.out.println(a);
        }
        assertTrue(map.isOccupied(new Vector2D(2, 0)));
        assertTrue(map.isOccupied(new Vector2D(3, 5)));
    }

    @Test
    public void simpleTest() {
        MoveDirection[] directions = new OptionsParser().parse("f f f f".split(" "));
        IWorldMap map = new RectangularMap(10, 5);
        Vector2D[] positions = { new Vector2D(0,0), new Vector2D(1,0) };
        IEngine engine = new SimulationEngine(directions, map, positions);
        for (int i = 0; i < directions.length; i++) {
            engine.run();
        }
        RectangularMap rectangularMap = (RectangularMap) map;
        for(Animal a : rectangularMap.getAnimals()) {
            System.out.println("SimTest1 " + a);
        }
        assertTrue(map.isOccupied(new Vector2D(0, 2)));
        assertTrue(map.isOccupied(new Vector2D(1, 2)));
    }

}
