package agh.ics.oop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapMoveDirectionTest {

    @Test
    public void testNextPrevious() {
        MapDirection dir = MapDirection.EAST;
        assertEquals(MapDirection.SOUTH, dir.next());
        assertEquals(MapDirection.NORTH, dir.previous());

        assertEquals(dir, dir.next().previous());
        assertEquals(dir, dir.previous().next());

        assertEquals(MapDirection.NORTH.next(), MapDirection.EAST);
        assertEquals(MapDirection.EAST.next(), MapDirection.SOUTH);
        assertEquals(MapDirection.SOUTH.next(), MapDirection.WEST);
        assertEquals(MapDirection.WEST.next(), MapDirection.NORTH);
    }

}
